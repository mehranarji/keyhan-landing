const gulp = require('gulp');
const sass = require("gulp-sass");
const browser = require("browser-sync");
const sourcemaps = require("gulp-sourcemaps");

const init = function (cb) {
  browser.init({
    server: './',
    open: false,
    port: 3000
  });

  cb();
}

const style = function(cb) {
  return gulp.src('asset/css/style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: 'compact' }).on('error', sass.logError))
    .pipe(sourcemaps.write('./', {includeContent: false}))
    .pipe(gulp.dest('asset/css'))
    .pipe(browser.stream({ match: '**/*.css' }));
}

const reload = function (cb) {
  browser.reload();

  cb();
};

const watch = gulp.series(init ,function (cb) {
  gulp.watch(['asset/css/**/*.scss'], style);
  gulp.watch(['asset/js/*.js'], reload);
  gulp.watch(['*.html'], reload);
  cb();
});

exports.default = watch;
exports.style = style;

