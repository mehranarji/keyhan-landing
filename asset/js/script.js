var scrollToTop = document.querySelector('.jump-to-top');
var introOverlay = document.querySelector('#section-intro .section-overlay');
/**
 * Float footer stuff
 */
document.querySelector(".bottom-float .btn-lg").onclick = function() {
    document.querySelector(".bottom-float").className = "bottom-float active";
};

/**
 * show scroll to bottom if user wont scroll more
 * than specific time
 */
setTimeout(function() {
    var scrollTop = document.querySelector('html').scrollTop;

    if (introOverlay!== null && scrollTop < 300) {
        introOverlay.classList.add('active');
    }
}, 3000);

/**
 * Listen Scroll
 */
document.querySelector('body').onscroll = function(ev) {
    var scrollTop = document.querySelector('html').scrollTop;

    if (introOverlay !== null && scrollTop > 300) {
        introOverlay.classList.remove('active');
    }

    if (scrollToTop !== null && scrollTop > 700) {
        scrollToTop.classList.add('active');
    } else {
        scrollToTop.classList.remove('active');
    }
}

// Default configuration
var sr = ScrollReveal({
    duration: 300,
    useDelay: 'always',
    delay: 500,
    opacity: 0,
    viewFactor: .6,
    origin: 'bottom',
    distance: '50%',
    reset: false
})

// Intro
sr.reveal('#section-intro h1, #section-intro .section-img, #section-intro h2, #section-intro .h3', {delay: 1000});
sr.reveal('#section-intro .section-img img', {
    scale: 0,
    delay: 1500,
    rotate: {
        z: 40
    }
});


// Section Spec
sr.reveal('#section-spec .section-img');
sr.reveal('#section-spec .section-img img', {
    scale: 0,
    delay: 700,
    rotate: {
        z: 40
    }
});
sr.reveal('#section-spec .section-icon', {
    origin: 'right',
    delay: 600
});
sr.reveal('#section-spec h3');

// Section feature
sr.reveal('#section-feature .section-img');
sr.reveal('#section-feature .section-img img', {
    scale: 0,
    delay: 700,
    rotate: {
        z: 40
    }
});
sr.reveal('#section-feature h3', {
    origin: 'left'
});
sr.reveal('#section-feature .section-icon', {
    origin: 'right',
    delay: 600
});

// Section price
sr.reveal('#section-price .section-img');
sr.reveal('#section-price .section-img img', {
    scale: 0,
    delay: 700,
    rotate: {
        z: 40
    }
});
sr.reveal('#section-price .h2', {
    origin: 'right'
});
sr.reveal('#section-price h3');
sr.reveal('#section-price .section-icon-lg', {
    origin: 'bottom',
    delay: 600
});

// Section video
sr.reveal('#section-video .section-img');
sr.reveal('#section-video .h4', {
    origin: 'right'
});
sr.reveal('#section-video .section-footer-img', {
    origin: 'bottom',
    viewFactor: 0
});

// Section gift
sr.reveal('#section-gift .section-img');
sr.reveal('#section-gift .section-img img', {
    scale: 0,
    delay: 700,
    rotate: {
        z: 40
    }
});
sr.reveal('#section-gift .h2', {
    origin: 'right'
});

// Section footer
sr.reveal('#section-footer .section-img');
sr.reveal('#section-footer .section-img img', {
    scale: 0,
    delay: 700,
    rotate: {
        z: 40
    }
});
sr.reveal('#section-footer .h4.bg-indigo');
sr.reveal('#section-footer .h2', {delay: 700});
sr.reveal('#section-footer .footer-form', {delay: 900});
